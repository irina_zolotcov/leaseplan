Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  @Positive @Run
  Scenario Outline: Search available products : positive scenarios
    When user search products by "<product>"
    Then user receive 200 HTTP response
    And user sees the results displayed
    Examples:
      | product |
      | orange  |
      | apple   |
      | pasta   |
      | cola    |

  @Negative @Run
  Scenario Outline: Search not available products : negative scenarios
    When user search products by "<product>"
    Then user receive <http_status> HTTP response
    And user see <error> error message
    Examples:
      | product | http_status | error     |
      | mango   | 404         | Not found |
      | @@@@    | 404         | Not found |
      | 1       | 404         | Not found |


