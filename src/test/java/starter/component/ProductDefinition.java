package starter.component;

import com.fasterxml.jackson.databind.type.CollectionType;
import io.restassured.mapper.ObjectMapper;
import io.restassured.mapper.ObjectMapperDeserializationContext;
import io.restassured.mapper.ObjectMapperSerializationContext;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class ProductDefinition {
    private static String provider;
    private static String title;
    private static String url;
    private static String brand;
    private static Long price;
    private static String unit;
    private static Boolean isPromo;
    private static String promoDetails;
    private static String image;


}
