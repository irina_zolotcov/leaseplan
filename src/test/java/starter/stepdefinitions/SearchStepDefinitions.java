package starter.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.internal.http.HTTPBuilder;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.ListAssert;
import org.eclipse.jetty.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Assert;
import starter.component.ProductDefinition;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static starter.utils.UtilsData.URL;

public class SearchStepDefinitions {

    @When("user search products by {string}")
    public void heCallsEndpoint(String product) {
        Response response = SerenityRest.given().get(URL + product);
        Serenity.setSessionVariable("resultOfSearch").to(response.jsonPath().get());
    }

    @Then("user receive {int} HTTP response")
    public void checkHttpStatus(int httpStatus) {
        restAssuredThat(response -> response.statusCode(httpStatus));
    }

    @Then("user sees the results displayed")
    public void checkDsplayedResults() {
        List<Object> resultOfSearch = Serenity.sessionVariableCalled("resultOfSearch");
        Assertions.assertThat(resultOfSearch).isNotNull();
        Assertions.assertThat(resultOfSearch.size()).isNotZero();
    }


    @And("user see {} error message")
    public void userSeeErrorErrorMessage(String errorMessage) {
        LinkedHashMap<String,LinkedHashMap<String,String >> resultOfSearch = Serenity.sessionVariableCalled("resultOfSearch");
        Assertions.assertThat(errorMessage).isEqualTo(resultOfSearch.get("detail").get("message").toString());
    }
}
